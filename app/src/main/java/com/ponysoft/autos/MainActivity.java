package com.ponysoft.autos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

     String color = "", totalPriceString;
    int carDefaultPrice;
    int carFamilyPrice = 10000;
    int carSportPrice = 30000;
    int totalPrice = 0;
    int aditionalPrice = 0;
    int metallicImport = 2000;
    int airImport = 3000;
    int alloyWheelsImport = 1000;
    int paintColor = 1750;  // Color Negro o Verde.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner brandSpinner = findViewById(R.id.brandSpinner);
        final RadioGroup modelRdGp = findViewById(R.id.modelRdGp);
        final Button calculateBtn = findViewById(R.id.calculateBtn);
        final EditText priceEdTx = findViewById(R.id.priceEdTx);
        final ListView colorListVw = findViewById(R.id.colorListVw);
        final ArrayList<String> carArrayList = new ArrayList<>();
        final ArrayList<Integer> priceArrayList = new ArrayList<>();
        final ArrayList<String> colorArrayList = new ArrayList<>();
        final CheckBox metallicCkBox = findViewById(R.id.metallicCkBox);
        final CheckBox airCkBox = findViewById(R.id.airCkBox);
        final CheckBox alloyWheelsCkBox = findViewById(R.id.alloyWheelsCkBox);
        final Button clearBtn = findViewById(R.id.clearBtn);

        Car car0 = new Car("BMW",50000);
        Car car1 = new Car("Ferrari",90000);
        Car car2 = new Car("Ford",10000);
        Car car3 = new Car("Honda",25000);
        Car car4 = new Car("Nissan",20000);
        Car car5 = new Car("Opel",30000);
        Car car6 = new Car("Porsche",85000);
        Car car7 = new Car("Renault",10500);
        Car car8 = new Car("Seat",11000);
        Car car9 = new Car("Toyota",35000);

        carArrayList.add(car0.getBrand());
        carArrayList.add(car1.getBrand());
        carArrayList.add(car2.getBrand());
        carArrayList.add(car3.getBrand());
        carArrayList.add(car4.getBrand());
        carArrayList.add(car5.getBrand());
        carArrayList.add(car6.getBrand());
        carArrayList.add(car7.getBrand());
        carArrayList.add(car8.getBrand());
        carArrayList.add(car9.getBrand());

        priceArrayList.add(car0.getPrice());
        priceArrayList.add(car1.getPrice());
        priceArrayList.add(car2.getPrice());
        priceArrayList.add(car3.getPrice());
        priceArrayList.add(car4.getPrice());
        priceArrayList.add(car5.getPrice());
        priceArrayList.add(car6.getPrice());
        priceArrayList.add(car7.getPrice());
        priceArrayList.add(car8.getPrice());
        priceArrayList.add(car9.getPrice());

        colorArrayList.add("Amarillo");
        colorArrayList.add("Azul");
        colorArrayList.add("Blanco");
        colorArrayList.add("Negro");
        colorArrayList.add("Rojo");
        colorArrayList.add("Verde");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, carArrayList);
        brandSpinner.setAdapter(adapter);
        brandSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                carDefaultPrice = priceArrayList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> colorAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, colorArrayList);
        colorListVw.setAdapter(colorAdapter);
        colorListVw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tv = (TextView) view;
                color = tv.getText().toString();
            }
        });
        //  Selecciona el Item por defecto(aun no funciona).
        //colorListVw.setItemChecked(colorAdapter.getPosition("Amarillo"),true);

        modelRdGp.check(R.id.classicRdBtn);

        calculateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (modelRdGp.getCheckedRadioButtonId() == R.id.classicRdBtn) {
                    aditionalPrice = 0;
                }
                if (modelRdGp.getCheckedRadioButtonId() == R.id.familyRdBtn) {
                    aditionalPrice = carFamilyPrice;
                }
                if (modelRdGp.getCheckedRadioButtonId() == R.id.sportRdBtn) {
                    aditionalPrice = carSportPrice;
                }

                if(!(metallicCkBox.isChecked()) &&
                        !(airCkBox.isChecked()) &&
                        !(alloyWheelsCkBox.isChecked())) {
                    totalPrice = carDefaultPrice + aditionalPrice;

                }else if(!(metallicCkBox.isChecked()) &&
                        !(airCkBox.isChecked()) &&
                        alloyWheelsCkBox.isChecked()) {
                    totalPrice = carDefaultPrice + aditionalPrice +
                            alloyWheelsImport;

                }else if(!(metallicCkBox.isChecked()) &&
                        airCkBox.isChecked() &&
                        !(alloyWheelsCkBox.isChecked())) {
                    totalPrice = carDefaultPrice + aditionalPrice +
                            airImport;

                }else if(!(metallicCkBox.isChecked()) &&
                        airCkBox.isChecked() &&
                        alloyWheelsCkBox.isChecked()) {
                    totalPrice = carDefaultPrice + aditionalPrice +
                            airImport + alloyWheelsImport;

                }else if(metallicCkBox.isChecked() &&
                        !(airCkBox.isChecked()) &&
                        !(alloyWheelsCkBox.isChecked())) {
                    totalPrice = carDefaultPrice + aditionalPrice +
                            metallicImport;

                } else if(metallicCkBox.isChecked() &&
                        !(airCkBox.isChecked()) &&
                        alloyWheelsCkBox.isChecked()) {
                    totalPrice = carDefaultPrice + aditionalPrice +
                            metallicImport + alloyWheelsImport;

                } else if(metallicCkBox.isChecked() &&
                        airCkBox.isChecked() &&
                        !(alloyWheelsCkBox.isChecked())) {
                    totalPrice = carDefaultPrice + aditionalPrice +
                            metallicImport + airImport;

                } else if(metallicCkBox.isChecked() &&
                        airCkBox.isChecked() &&
                        alloyWheelsCkBox.isChecked()) {
                    totalPrice = carDefaultPrice + aditionalPrice +
                            metallicImport + airImport + alloyWheelsImport;
                }

                if((color.equals("Negro")) || (color.equals("Verde"))) {
                    int temp = totalPrice;
                    totalPrice = temp + paintColor;
                }

                totalPriceString = String.valueOf(totalPrice);
                priceEdTx.setText(totalPriceString);
            }
        });

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                totalPrice = 0;
                totalPriceString = String.valueOf(totalPrice);
                priceEdTx.setText(totalPriceString);
            }
        });

    }
}
